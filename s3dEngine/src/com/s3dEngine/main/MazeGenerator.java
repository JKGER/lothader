package com.s3dEngine.main;

import java.io.IOException;
import java.util.Arrays;

/*
 * Der Maze-Generator wurde vollst�ndig alleine erstellt und nirgendswo auch nur ansatzweise kopier/abgeschaut.
 * Alles hier wurde von Jan Bartels erstellt und ist eine Eigenleistung.
 */

public class MazeGenerator {
	
	//Image als Png
	MazeConverter mazeconverter;
	
	//output:
	//System.out.println(Arrays.deepToString(maze));
	private int mHeight, mWidth;
	public int[][] maze;
	private int material = 3;
	
	public MazeGenerator(int height, int width) {

		mHeight = height;
		mWidth = width;
		maze = new int[height][width];
		createMaze(height,width);
	}
	
	private int[][] createMaze(int mapHeight, int mapWidth){
		maze = createCanvas(mapHeight,mapWidth);
		maze = createSquare(0,0,mapHeight,mapWidth);
		maze = createBorder(mapHeight,mapWidth);
		return maze;
	}
	
	private int[][] createCanvas(int mapHeight,int mapWidth){
				
		for(int i=0;i<mapHeight;i++) {
			for(int j = 0;j<mapWidth;j++) {
				maze[i][j] = 0;
			}
		}
		return maze;
	}
	
	private int[][] createBorder(int mapHeight, int mapWidth) {
		for(int x = 0; x<mapWidth;x++) {
			maze[0][x] = 1;
			maze[mapHeight-1][x] = 1;
		}
		for(int y = 0; y<mapWidth;y++) {
			maze[y][0] = 1;
			maze[y][mapWidth-1] = 1;
		}	
		if(Game.getOutput) System.out.println(Arrays.deepToString(maze));
		System.out.println("Labyrinth erstellt");
		return maze;
	}
	
	private boolean checkSpace(int y,int x,int ymax,int xmax,int yWall, int xWall) {
		if(yWall>y+1 && xWall>x+1 && yWall < ymax-1 && xWall < xmax-1) {
			return true;
		}else return false;
	}

	
	public int[][] createSquare(int y, int x, int ymax, int xmax) {
		
		boolean done;
		int ySpace = ymax-y;
		int xSpace = xmax-x;
		if(Game.getOutput) System.out.println("ySpace=" + ySpace + " ;xSpace= " + xSpace);
		
		if(ySpace>=4 && xSpace>=4) {
			do {
				done = false;
				//Erstellt Mauer 
				int yWall = (int)(Math.random()*(y+ySpace));
				int xWall = (int)(Math.random()*(x+xSpace));
				if(yWall == mHeight-1 || xWall == mWidth-1) {
					yWall--;
					xWall--;
				}
				//Checkt, ob der Abstand eingehalten wird
				if(!checkSpace(y,x,ymax,xmax,yWall,xWall)) {
					done = false;
				}else {
					for(int k = y; k<y+ySpace;k++) {
						maze[k][xWall] = material;
					}
					for(int l = x; l<x+xSpace;l++) {
						maze[yWall][l] = material;
					}
					done = true;	
					
					if(Game.getOutput) System.out.println("yWall=" + yWall + " ;xWall= " + xWall);
				
					createHole(y,x,ySpace,xSpace,yWall,xWall);
				}		
			}while(!done);
		}else {
			if(Game.getOutput) System.out.println("Endet bei createSquare, da x oder y Space zu klein sind");
			setSpawn();
			setgoal();
			if(Game.getOutput) System.out.println(Arrays.deepToString(maze).replace("],", "]\n"));
			return maze;
		}
		return maze;
	}
		
	//Nummer der Wand:
	public int[][] createHole(int y, int x,int ySpace, int xSpace, int yWall, int xWall){
		boolean success;
		
		do{
			success=false;
			
			int wallone = (int)(Math.random()*3)+1;
			int walltwo = (int)(Math.random()*3)+1;
			int wallthree = (int)(Math.random()*3)+1;
			
			if(wallone == walltwo || wallone == wallthree || walltwo == wallthree ) {
				success = false;
			}else {
				if(Game.getOutput) System.out.println("1: " + wallone + " 2: " + walltwo + " 3: " + wallthree);
				
				//L�cher
				if(Game.getOutput) System.out.println("L�cher bei Wand: " + wallone + ";" + walltwo + ";" + wallthree);
				actualhole(y,x,ySpace,xSpace,yWall,xWall,wallone);
				actualhole(y,x,ySpace,xSpace,yWall,xWall,walltwo);
				actualhole(y,x,ySpace,xSpace,yWall,xWall,wallthree);
				finishedHoles(y,x,ySpace,xSpace,yWall,xWall);

				
				success = true;
			}
			
			
		}while(!success);
			return maze;
	}

	//Erstellt die L�cher
	private int[][] actualhole(int y, int x,int ySpace,int xSpace, int yWall, int xWall, int wallnumber){
		if(Game.getOutput) System.out.println("Versucht es ");

		switch (wallnumber) {	
		case 1:
			boolean fertig;
			
			do {
				int holeone = (int) (Math.random()*(mHeight));
				fertig = false;
				
				if(holeone < yWall) {
					
					fertig = true;
					if(Game.getOutput) System.out.println("Loch in Wand " + wallnumber + " unter " + holeone);
					maze[holeone][xWall] = 0;
					maze[holeone+1][xWall] = 0;
					
				}else fertig = false;
				
			}while(!fertig);
			break;
			
			
		case 2:
			boolean fertigtwo;
			
			do {
				int holetwo = (int) (Math.random()*(mWidth));
				fertigtwo = false;
				
				if(holetwo>xWall && holetwo<= x+xSpace) {
					
						fertigtwo = true;
						if(Game.getOutput) System.out.println("Loch in Wand " + wallnumber + " unter " + holetwo);
						maze[yWall][holetwo] = 0;	
						maze[yWall][holetwo-1] = 0;	
					
					}else fertigtwo = false;	
				
			}while(!fertigtwo);
			break;
			
		case 3:
			boolean fertigthree;
			
			do {
				int holethree = (int) (Math.random()*(mHeight));
				fertigthree = false;
				
				if(holethree > yWall && holethree <= y+ySpace) {
					fertigthree = true;
					if(Game.getOutput) System.out.println("Loch in Wand " + wallnumber + " unter " + holethree);
					maze[holethree][xWall] = 0;	
					maze[holethree-1][xWall] = 0;	
					
				}else fertigthree = false;	
				
			}while(!fertigthree);
			break;
			
		case 4:
			boolean fertigfour;
			do {
				int holefour = (int) (Math.random()*(mWidth));
				fertigfour = false;
				
				if(holefour < xWall) {
					
					fertigfour = true;
					if(Game.getOutput) System.out.println("Loch in Wand " + wallnumber + " unter " + holefour);
					maze[yWall][holefour] = 0;
					maze[yWall][holefour-1] = 0;
					
				}else fertigfour = false;
				
			}while(!fertigfour);
			break;
		}
		return maze;
	}
	
	private void finishedHoles(int y,int x, int ySpace, int xSpace, int yWall, int xWall) {
		// xSpace = xmax-x;
		int xmax = xSpace+x;
		int ymax = ySpace+y;
		
		if(yWall >y+2 && xWall >x+2) {
			if(Game.getOutput) System.out.println("Kammer 1");
			createSquare(y, x, yWall, xWall);
		}
		if(yWall<ymax-4 && xWall>x+2){
			if(Game.getOutput) System.out.println("Kammer 3");
		}
		if(yWall>y+2 && xWall<xmax-2) {
			if(Game.getOutput) System.out.println("Kammer 2");
			createSquare(y,xWall,yWall,x+xSpace);
		}
		if(yWall<ySpace+y-2 && xWall<x+xSpace-2) {
			createSquare(yWall,xWall,y+ySpace,x+xSpace);
			
		}
		else {
			setSpawn();
			setgoal();
			if(Game.getOutput) System.out.println("Maze erfolgreich geladen");
		}		
	}
	private void setgoal() {
		maze[mHeight-2][mWidth-2] = 5;
		
	}

	private void setSpawn() {
		maze[Game.yPos][Game.xPos] = 0;
		try {
			mazeconverter = new MazeConverter(mHeight, mWidth,maze);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
