package com.s3dEngine.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

/*
 * Wichtig:
 * Dieser Code wurde in geringen Maen von Lode's Computer Graphics Tutorial bernommen.
 * Die States und alle weiteren Fenster, Generatoren, Musikklassen usw. wurden vollstndig alleine erstellt.
 * Weiterhin sind die Texturen selbst erschaffen.
 * Das grobe Prinzip wurde allerdings kopiert.
 * 
 * Hier geht es um Tutorial: https://lodev.org/cgtutor/raycasting.html
 *  */

public class Game extends JFrame implements Runnable{
	
	public static enum STATE{
		MENU,
		GAME,
		PAUSE,
		FINISHED,
		SETTINGS;
	};
	
	public static enum MODE{
		GIVEN,
		GENERATED,
	};
	
	//Private
	private static final long serialVersionUID = 1L;
	private Thread thread;
	private boolean running;
	private Timer renderTimer;
	private static Timer gameTimer;
	private Sound mainSong;
	
	//Graphics
	private BufferStrategy bs;
	private BufferedImage image;
	private Graphics g;
	
	//States
	public static STATE state;
	public static MODE mode;
	
	//Players Position
	public static final int yPos = 2;
	public static final int xPos = 2;
	
	//Public
	public static final int wHeight = 1280;
	public static final int wWidth = wHeight/9 * 12;
	public int mapWidth = 100;
	public int mapHeight = 100;
	public int[] pixels;
	public static int currentTime = 0;
	public static int targetTime = 100 + 20;
	
	//Booleans 
	public static final boolean getOutput = false;
	public static boolean isBlack = false;
	
	public MazeGenerator mazegenerator;
	public ArrayList<Texture> textures;
	public Camera camera;
	public Screen screen;
	public Weapon weapon;
	public Menu menu;
	public SettingsState settings;
	public Pause pause;
	public Finish finish;

	
	public static int[][] map = 
		{
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,4},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,2},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,2},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,2},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,2},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,2},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,2},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,4},
			{1,3,4,1,2,3,4,0,3,0,0,0,0,0,4},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,4},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,0,4},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,4},
			{1,0,0,0,0,0,0,0,3,0,0,0,0,5,4},
			{1,1,1,1,1,1,1,4,4,4,4,4,4,4,4}
		};


		// TimerTasks / Rendering / Countdown

		public class GameTask extends TimerTask
		{
			@Override
			public void run()
			{
				if(Game.state == STATE.GAME)
				{
					Game.currentTime++;
					if(Game.currentTime == Game.targetTime)
					{
						Game.currentTime = 0;
						Game.state = STATE.MENU;
						mainSong.stop();						
					}
				}
			}
		}

		public class TickTask extends TimerTask
		{
			@Override
			public void run()
			{
				render();
				if (currentTime == 25) {
				    mainSong.loop();
				}
			}
		}
	
	public Game() {
		/** Zwei Timer, da ein Timer einen Thread hat und mehrere Tasks also sequentiell 
		 * 	bearbeitet werden werden */
		renderTimer = new Timer();
		gameTimer = new Timer();
		
		//State
		state = STATE.MENU;
		mode = MODE.GENERATED;
		
		thread = new Thread(this);
		image = new BufferedImage(wWidth, wHeight, BufferedImage.TYPE_INT_RGB);
		
		//Texure 
		textures = new ArrayList<Texture>();		
		textures.add(Texture.WOOD);
		textures.add(Texture.BRICK);
		textures.add(Texture.COBBLESTONE);
		textures.add(Texture.STONEBRICK);
		textures.add(Texture.BROCKENWALL);
		
		//Maze
		mazegenerator = new MazeGenerator(mapHeight,mapWidth);
		if(mode == MODE.GENERATED ) {
		map = mazegenerator.maze;
		}
		
		//Instance
		camera = new Camera(yPos, xPos, 1, 0, 0, -.66);
		screen = new Screen(map, mapWidth, mapHeight, textures, wWidth, wHeight);
		weapon = new Weapon(map,mapWidth,mapHeight,textures,wWidth,wHeight);
		menu = new Menu(wWidth, wHeight);
		pause = new Pause(wWidth,wHeight);
		finish = new Finish(wWidth,wHeight);
		settings = new SettingsState();
		mainSong = new Sound("Song.wav");
		
		
		this.addMouseListener(new MouseInput());
		this.addKeyListener(new InputHandler());

		//Window
		setSize(wWidth, wHeight);
		setResizable(false);
		setTitle("Lothader");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBackground(Color.black);
		setLocationRelativeTo(null);
		setVisible(true);
		start();
	}
	
	
	private synchronized void start() {
		running = true;
		thread.start();
	}
	public synchronized void stop() {
		running = false;
		try {
			thread.join();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	public void render() {
		bs = getBufferStrategy();
		if(bs == null) {
			createBufferStrategy(3);
			return;
		}
		g = bs.getDrawGraphics();
		
		//Hier wird gezeichnet
		
		g.setColor(Color.black);
		g.fillRect(0, 0, wWidth, wHeight);
		
		if(state == STATE.GAME) {
			//Konvertiert das image in Pixel
			pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
			screen.update(camera, pixels);
			weapon.update(camera, pixels);
			camera.update(map);
			
			g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
		}
		if(state == STATE.MENU) {
			menu.update(g);
		}
		if(state == STATE.PAUSE) {
			pause.update(g);
		}
		if(state == STATE.FINISHED) {
			finish.update(g);
		}
		if(state == STATE.SETTINGS) {
			map = settings.setSize();
			mapHeight = settings.mazeproportions;
			mapWidth = settings.mazeproportions;
			screen = new Screen(map, mapWidth, mapHeight, textures, wWidth, wHeight);
			weapon = new Weapon(map,mapWidth,mapHeight,textures,wWidth,wHeight);
			state = Game.STATE.MENU;
			setTime(mapHeight);
		}
		

		
		bs.show();

	}
	
	
	public void run() {
		requestFocus();
		renderTimer.scheduleAtFixedRate(new TickTask(), 0, 1000/60);
		gameTimer.scheduleAtFixedRate(new GameTask(), 0, 1000);
	}
	
	private void setTime(int mazeWidth) {
		targetTime = mazeWidth + 20;
	}
	
	public static void main(String [] args) {

		new Game();
//		MazeGenerator mazecreator = new MazeGenerator(15, 15);
	}
}





