package com.s3dEngine.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

public class Finish {

	private int wWidth,wHeight;

	
	public Finish(int wWidth,int wHeight) {
		
		this.wWidth = wWidth;
		this.wHeight = wHeight;	
		
	}
	

	public void update(Graphics g) {
		
		if(!Game.isBlack) g.setColor(Color.DARK_GRAY);g.fillRect(0, 0, wWidth, wHeight);
		
		Font fnt0 = new Font("arial", Font.BOLD, 120);
		g.setFont(fnt0);
		g.setColor(Color.white);
		g.drawString("Fertig", wWidth/2-200, wHeight/2-250-100);

		
	}
	
}
