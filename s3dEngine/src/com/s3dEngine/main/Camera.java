package com.s3dEngine.main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import com.s3dEngine.main.InputHandler;

public class Camera{
	
	//Public 
	public double xPos,yPos,xDir,yDir,xPlane,yPlane;	//Pos.y/.x = Players Position - Dir = welche Richtung er guckt - Plane = SichtVektor nach Rechts,Links, aber auch FOV (0.66)
	public boolean left,right,forward,backward,shoot,pause;
	public final double MOVE_SPEED = 0.08;
	public final double ROTATION_SPEED = .025;

	//konstruktor
	public Camera(double x, double y, double xd, double yd, double xp, double yp) {
		//Position der Spielers 
		xPos = x;
		yPos = y;
		//Sichtrichung 
		xDir = xd;
		yDir = yd;
		//90 Grad zur Sichtrichtung - beschr�nkung
		xPlane = xp;
		yPlane = yp;
		
		pause=false;
	}

	
	
	public void update(int[][] map) {
		
		if(InputHandler.pressedKeys.contains(KeyEvent.VK_W)) {
			
			//Diese Funktion checkt, ob der int, auf den er guckt 0 ist... wenn ungleich null stoppt er
			if(map[(int)(xPos+xDir*MOVE_SPEED)][(int)yPos] == 0) {		//casten auf int 
				xPos+=xDir*MOVE_SPEED;
			}
			if(map[(int)xPos][(int)(yPos + yDir*MOVE_SPEED)] == 0) {
				yPos+=yDir*MOVE_SPEED;
			}
		}
		
		//Genau das gleiche - nur nach hinten
		if(InputHandler.pressedKeys.contains(KeyEvent.VK_S)) {
			if(map[(int)(xPos-xDir*MOVE_SPEED)][(int)yPos]==0) {
				xPos-=xDir*MOVE_SPEED;
			}
			if(map[(int)xPos][(int)(yPos-yDir*MOVE_SPEED)] == 0) {
				yPos-=yDir*MOVE_SPEED;
			}
		}
		
		/*
		 * RotationsMatrix - Online
		 * 	[ cos(ROTATION_SPEED) -sin(ROTATION_SPEED) ]
			[ sin(ROTATION_SPEED)  cos(ROTATION_SPEED) ]
		 */
		
		
		if(InputHandler.pressedKeys.contains(KeyEvent.VK_D)) {
			
			//Rotiert die Sichtrichtung
			double oldxDir=xDir;
			xDir=xDir*Math.cos(-ROTATION_SPEED) - yDir*Math.sin(-ROTATION_SPEED);
			yDir=oldxDir*Math.sin(-ROTATION_SPEED) + yDir*Math.cos(-ROTATION_SPEED);
			
			//Ebene rotiert sich mit
			double oldxPlane = xPlane;
			xPlane=xPlane*Math.cos(-ROTATION_SPEED) - yPlane*Math.sin(-ROTATION_SPEED);
			yPlane=oldxPlane*Math.sin(-ROTATION_SPEED) + yPlane*Math.cos(-ROTATION_SPEED);
		}
		if(InputHandler.pressedKeys.contains(KeyEvent.VK_A)) {
			//Selbes rotationsprinzip
			double oldxDir=xDir;
			xDir=xDir*Math.cos(ROTATION_SPEED) - yDir*Math.sin(ROTATION_SPEED);
			yDir=oldxDir*Math.sin(ROTATION_SPEED) + yDir*Math.cos(ROTATION_SPEED);
			
			//Auch hier lediglich eine rotation
			double oldxPlane = xPlane;
			xPlane=xPlane*Math.cos(ROTATION_SPEED) - yPlane*Math.sin(ROTATION_SPEED);
			yPlane=oldxPlane*Math.sin(ROTATION_SPEED) + yPlane*Math.cos(ROTATION_SPEED);
		}
	}
}













