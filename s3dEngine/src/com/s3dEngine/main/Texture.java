package com.s3dEngine.main;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class Texture {

	//Texturen
	public static Texture WOOD = new Texture("planks_birch.png",128);
	public static Texture BRICK = new Texture("brick.png",128);
	public static Texture COBBLESTONE = new Texture("cobblestone.png",128);
	public static Texture STONEBRICK = new Texture("stonebrick.png",128);
	public static Texture BROCKENWALL = new Texture("brockenwall.png",64);

	//Public
	public int[] pixels;
	public final int size; 		//128*128
	
	//Private 
	private URL location;
	private ClassLoader loader = ClassLoader.getSystemClassLoader();
	
	//Konstruktor
	public Texture(String name, int size) {
		location = loader.getResource(name);
		this.size = size;
		pixels = new int[this.size*this.size];
		load();
	}

	private void load() {
		try {
			BufferedImage image = ImageIO.read(location);
			int w = image.getWidth();
			int h  = image.getHeight();
			image.getRGB(0, 0,w,h,pixels,0,w);	//Speichert Daten von BufferedImage als Pixel
			
			}
		catch(IOException e) {
				e.printStackTrace();
			}
	}
	
}
