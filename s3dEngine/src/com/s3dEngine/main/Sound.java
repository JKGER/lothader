package com.s3dEngine.main;

import java.net.URL;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

//Source: https://stackoverflow.com/questions/11919009/using-javax-sound-sampled-clip-to-play-loop-and-stop-multiple-sounds-in-a-game


public class Sound {
	
	private Clip clip;
	
	public Sound(String name) {
		URL location = ClassLoader.getSystemResource(name);
		try {
			AudioInputStream sound = AudioSystem.getAudioInputStream(location);
            clip = AudioSystem.getClip();
            clip.open(sound);
            }     
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void play() {
		clip.setFramePosition(0);
		clip.start();
		if(Game.getOutput) System.out.println("Sound played.");
		
	}
	
    public void stop(){
        clip.stop();
    }
    
    public void loop(){
    	clip.setFramePosition(0);
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }
}
