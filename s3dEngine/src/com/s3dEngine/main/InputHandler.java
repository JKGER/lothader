package com.s3dEngine.main;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;

import com.s3dEngine.main.Game.STATE;

public class InputHandler implements KeyListener {
	
	public static List<Integer> pressedKeys = new ArrayList<Integer>();
	public static boolean shoot = false;
	
	public InputHandler(){
		
	}
	
	@Override
	public void keyPressed(KeyEvent key) {
		
		if((key.getKeyCode() == KeyEvent.VK_A)) {
			if(!pressedKeys.contains(KeyEvent.VK_A))
			pressedKeys.add(KeyEvent.VK_A);
		}
		if((key.getKeyCode() == KeyEvent.VK_D)) {
			if(!pressedKeys.contains(KeyEvent.VK_D))
			pressedKeys.add(KeyEvent.VK_D);
		}
		if((key.getKeyCode() == KeyEvent.VK_W)) {
			if(!pressedKeys.contains(KeyEvent.VK_W))
			pressedKeys.add(KeyEvent.VK_W);
		}
		if((key.getKeyCode() == KeyEvent.VK_S)) {
			if(!pressedKeys.contains(KeyEvent.VK_S))
			pressedKeys.add(KeyEvent.VK_S);
		}
		if((key.getKeyCode() == KeyEvent.VK_P)) {
			if(!pressedKeys.contains(KeyEvent.VK_P))
			pressedKeys.add(KeyEvent.VK_P);
			if(Game.state != Game.STATE.PAUSE) {
				Game.state = Game.STATE.PAUSE;
			}else {
				Game.state = Game.STATE.GAME;
			}
		}
		if((key.getKeyCode() == KeyEvent.VK_SPACE)) {
			if(!pressedKeys.contains(KeyEvent.VK_SPACE)) {
				pressedKeys.add(KeyEvent.VK_SPACE);
				shoot = true;
			
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent key) {
		
		if((key.getKeyCode() == KeyEvent.VK_A))
			pressedKeys.remove(pressedKeys.indexOf(KeyEvent.VK_A));
		if((key.getKeyCode() == KeyEvent.VK_D)) 
			pressedKeys.remove(pressedKeys.indexOf(KeyEvent.VK_D));
		if((key.getKeyCode() == KeyEvent.VK_W))
			pressedKeys.remove(pressedKeys.indexOf(KeyEvent.VK_W));
		if((key.getKeyCode() == KeyEvent.VK_S))
			pressedKeys.remove(pressedKeys.indexOf(KeyEvent.VK_S));
		if((key.getKeyCode() == KeyEvent.VK_P))
			pressedKeys.remove(pressedKeys.indexOf(KeyEvent.VK_P));
		if((key.getKeyCode() == KeyEvent.VK_SPACE))
			pressedKeys.remove(pressedKeys.indexOf(KeyEvent.VK_SPACE));
			shoot = false;

		}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}