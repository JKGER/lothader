package com.s3dEngine.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Pause {
	
	private int w,h;
	
	public Pause(int wWidth, int wHeight) {
		this.w = wWidth;
		this.h = wHeight;
		
	}
	
	public void update(Graphics g) {
		
		if(!Game.isBlack) g.setColor(Color.DARK_GRAY);g.fillRect(0, 0, w, h);
		
		//Kreis
		g.setColor(Color.WHITE);
		g.fillOval(w/2-250, h/2-250, 500, 500);
		//Pause
		g.setColor(Color.BLACK);
		g.fillRect(w/2-125, h/2-125, 100, 250);
		g.fillRect(w/2+25, h/2-125, 100, 250);
		//Text
		Font fnt0 = new Font("arial", Font.BOLD, 120);
		g.setFont(fnt0);
		g.setColor(Color.white);
		g.drawString("Pause", w/2-200, h/2-250-100);
	}
}
