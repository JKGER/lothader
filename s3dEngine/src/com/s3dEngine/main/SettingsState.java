package com.s3dEngine.main;

import javax.swing.JOptionPane;

public class SettingsState{
	
	public int mazeproportions;
	private int[][] mazesize;
	private MazeGenerator mazegenerator;
	private String[] errormessage = {"Bitte positive Zahlen","Versuch es nochmal","Ups! Etwas ist schiefgelaufen","Leider nochmal","Komm schon, du kannst es","Bin ich ein Witz für dich","Nimm mich ernst"};
	int n = 0;
	
	public SettingsState() {
		
	}

	public int[][]  setSize() {
		
		
		String eingabe = JOptionPane.showInputDialog(null, "Geben Sie die Labyrinthgröße ein", "Labyrinthgröße",JOptionPane.PLAIN_MESSAGE);
		
		if(eingabe == null) {
			setSize();
		}else {
			
		    try
		    {
		      //Casten
		      mazeproportions = Integer.parseInt(eingabe.trim());
		      if(mazeproportions >=5) {
		    	  mazegenerator = new MazeGenerator(mazeproportions, mazeproportions);
		    	  mazesize = mazegenerator.maze;
		      }else {
		    	  JOptionPane.showMessageDialog(null, errormessage[n],"Flasche Eingabe", JOptionPane.WARNING_MESSAGE);
		    	  n++;
		    	  setSize();
		      }
		 
		    }
		    //Falls keine Zahl eingegeben wurde
		    catch (NumberFormatException nfe)
		    {
		      System.out.println("NumberFormatException: " + nfe.getMessage());
		      JOptionPane.showMessageDialog(null, "Bitte nur Zahlen","Lothader Fehler", JOptionPane.WARNING_MESSAGE);
		      setSize();
		    }
			
		}
		
	    return mazesize;
	}
	
	

}
