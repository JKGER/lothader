package com.s3dEngine.main;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseInput implements MouseListener{

	public Sound intro;
	
	
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		int mx = e.getX();
		int my = e.getY();
		/*
		 * 	public Rectangle playButton = new Rectangle(Game.wWidth/2-200,350,400,200);
		 */
		
		//Knopf Start
		if(mx >= Game.wWidth/2-200 && mx<= Game.wWidth/2 +200 && Game.state == Game.STATE.MENU) {
			if(my >= 350 && my<= 550) {
				Game.state = Game.STATE.GAME;
				Menu.menuMusic.stop();
				intro = new Sound("Intro.wav");
					
				intro.play();
				
			}
		}
		//Mode 
		if(mx >= Game.wWidth/2-200 && mx <= Game.wWidth/2+200 && Game.state == Game.STATE.MENU) {
			if(my >= 650 && my<= 850) {
				Game.state = Game.STATE.SETTINGS;
				
			}
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
