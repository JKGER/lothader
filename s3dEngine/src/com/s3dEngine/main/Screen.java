package com.s3dEngine.main;

import java.util.ArrayList;
import java.awt.Color;

/*
 * Wichtig:
 * Dieser Code wurde von Lode's Computer Graphics Tutorial �bernommen. Die �bersetzung in Java ist gr��enteils selber gemacht wurde, obwohl die Variablennamen kopiert wurden, der Einfachheit halber. 
 * Die Kommentare sind allerdings selber erstellt wurden, sodass jmd. neues den Code schnell verstehen kann.
 * 
 * Hier geht es um Tutorial: https://lodev.org/cgtutor/raycasting.html
 *  */

public class Screen {
	
	//Public
	public int[][] map;
	public int mapWidth, mapHeight, width, height;
	public ArrayList<Texture> textures;
	
	
	//Konstruktor
	public Screen(int[][] m, int mapW, int mapH, ArrayList<Texture> tex, int w, int h) {
		map = m;
		mapWidth = mapW;
		mapHeight = mapH;
		textures = tex;
		width = w;
		height = h;
	}
	
	//Bekommt ein objekt der camera und die mit dem Bild verbundene Pixel
	public int[] update(Camera camera, int[] pixels) {
		
		//obere Hlfte des Bildschirmes nach oben alles Schwarz anmalen
		for(int n=0; n<pixels.length/2; n++) {
			if(pixels[n] != Color.DARK_GRAY.getRGB()) pixels[n] = Color.DARK_GRAY.getRGB();
		}
		
		//untere anmalen
		for(int i=pixels.length/2; i<pixels.length; i++){
			if(pixels[i] != Color.gray.getRGB()) pixels[i] = Color.gray.getRGB();
		}
		
		//For-loop geht alle Pixel in x Richtung durch 
	    for(int x=0; x<width; x=x+1) {
			double cameraX = 2 * x / (double)(width) -1;
			
			/*
			 * der finale Ray in die x-Richtung
			 * Camera.xPlane kann auch 0 sein
			 */
		    double rayDirX = camera.xDir + camera.xPlane * cameraX;
		    double rayDirY = camera.yDir + camera.yPlane * cameraX;
		    
		    
		    /*
		     * Die Camera Position
		     */
		    int mapX = (int)camera.xPos;
		    int mapY = (int)camera.yPos;
		    
		    
		    /*
		     * L�nge des Rays von der Position zum nchsten x oder y Raster
		     */
		    double sideDistX;
		    double sideDistY;
		    
		    
		    //Lnge vom 1x bis zum 2ten x Raster, durch der der Ray geht -s.Skizze
		    double deltaDistX = Math.sqrt(1 + (rayDirY*rayDirY) / (rayDirX*rayDirX));
		    double deltaDistY = Math.sqrt(1 + (rayDirX*rayDirX) / (rayDirY*rayDirY));
		    
		    /*
		     * Distanz zur Wand 
		     */
		    double perpWallDist;
		    
		    
		    
		    /*
		     * Beschreibt die Richtung des Rays - In Positive X oder negative Richtung
		     *  x pos = 1, x neg = -1
		     */
		    int stepX, stepY;
		    
		    /*
		     * Wann wird die erste Wand getroffen?
		     * hit = true oder 1
		     */
		    boolean hit = false;
		    
		    /*
		     * Side variiert: Side=0 - auf der X-Achse eine Wand
		     * Side = 1 - auf y-Achse
		     */
		    int side=0;
		    
		    /*
		     * Wenn ich in positive Richtung gucke, ist der Ray positiov, also >0
		     */
		    if (rayDirX < 0)
		    {
		    	/*
		    	 * Folglich ist hierdie Richung negativ und der Step zum nchsten Feld ist -1
		    	 */
		    	stepX = -1;
		    	
		    	/*
		    	 * Zur nchsten x-Achse
		    	 */
		    	sideDistX = (camera.xPos - mapX) * deltaDistX;
		    }
		    else
		    {
		    	stepX = 1;
		    	sideDistX = (mapX + 1.0 - camera.xPos) * deltaDistX;
		    }
		    if (rayDirY < 0)
		    {
		    	stepY = -1;
		        sideDistY = (camera.yPos - mapY) * deltaDistY;
		    }
		    else
		    {
		    	stepY = 1;
		        sideDistY = (mapY + 1.0 - camera.yPos) * deltaDistY;
		    }
		    
		    //Loop to find where the ray hits a wall
		    while(!hit) {
		    	/*
		    	 * Sprung zum nhsten Quadrat - Kasten
		    	 */
		    	if (sideDistX < sideDistY)
		        {
		    		
		    		sideDistX += deltaDistX;
		    		mapX += stepX;
		    		//Side = 0, da hier die x-Achse getroffen wird
		    		side = 0;
		        }
		        else
		        {
		        	sideDistY += deltaDistY;
		        	mapY += stepY;
		        	side = 1;
		        }
		    	
		    	/*
		    	 * guck im Array der Karte nach, ob der Ray noch auf 0 ist
		    	 * Der Schnittpunkt mit einer Wand wird hiermit herausgefunden
		    	 */
		    	if(map[mapX][mapY] > 0) hit = true;
		    }
		    /*
		     * Distanz zur Wand 
		     */
		    if(side==0)
		    	perpWallDist = Math.abs((mapX - camera.xPos + (1 - stepX) / 2) / rayDirX);
		    else
		    	perpWallDist = Math.abs((mapY - camera.yPos + (1 - stepY) / 2) / rayDirY);	
		    
		    /*
		     * Nachdem man die Entfernung hat, kann man die Hhe der Wand ausrechnen, die ausgegeben werden soll
		     */
		    int lineHeight;
		    
		    /*
		     * wenn er nicht gerade davor steht - 1280(height)/Distanz ist die Proportion
		     */
		    if(perpWallDist > 0) lineHeight = Math.abs((int)(height / perpWallDist));
		    else lineHeight = height;

		    /*
		     * Obersten und untersten Pixel des Screens ausrechnen, in dem die Textur sein soll
		     * + height/2, damit das Objekt mittig im Screen ist 
		     */
		    int drawStart = -lineHeight/2+ height/2;
		    if(drawStart < 0)
		    	drawStart = 0;
		    int drawEnd = lineHeight/2 + height/2;
		    if(drawEnd >= height) 
		    	drawEnd = height - 1;

		    /*
		     * Texture hinufgen
		     */
		    int texNum = map[mapX][mapY] -1;
		    /*
		     * Punkt, in dem die Wand getroffen wurde
		     */
		    double wallX;
		    if(side==1) {//y-Wand
		    	/*
		    	 * auch camera.xPos + perpwallDist, aber als Konstanten Wert - als Spa darf man auch die // Entfernen:
		    	 */
//		    	wallX = (camera.xPos + ((mapY - camera.yPos + (1 - stepY) / 2) / rayDirY) * rayDirX);
		    	//geht folglich auch so
		    	wallX = camera.xPos + perpWallDist*rayDirX;
		    } else {//x-Wand
		    	wallX = (camera.yPos + ((mapX - camera.xPos + (1 - stepX) / 2) / rayDirX) * rayDirY);
//		    	wallX = camera.yPos + perpWallDist;
		    }
		    /*
		     * Abrunden auf Int - da Position ein int sein muss
		     */
		    wallX-=Math.floor(wallX);
		    /*
		     * X-Koordinate der textur
		     */
		    int texX = (int)(wallX * (textures.get(texNum).size));
		    if(side == 0 && rayDirX > 0) texX = textures.get(texNum).size - texX - 1;
		    if(side == 1 && rayDirY < 0) texX = textures.get(texNum).size - texX - 1;
		    /*
		     * Selbe mit y-Koordinate
		     */
		    for(int y=drawStart; y<drawEnd; y++) {
		    	int texY = (((y*2 - height + lineHeight) << 6) / lineHeight) / 2;
		    	int color;
		    	if(side==0) color = textures.get(texNum).pixels[texX + (texY * textures.get(texNum).size)];
		    	else color = (textures.get(texNum).pixels[texX + (texY * textures.get(texNum).size)]>>1) & 8355711;//y Texture dunkler
		    	pixels[x + y*(width)] = color;
		    }
		}
		return pixels;
	}
}




