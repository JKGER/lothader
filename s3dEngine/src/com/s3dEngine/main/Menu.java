package com.s3dEngine.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Menu {
	
	private int w, h;	//Fenster Weite und H�he
	
	//Kn�pfe
	public Rectangle playButton = new Rectangle(Game.wWidth/2-200,350,400,200);
	public Rectangle modeButton = new Rectangle(Game.wWidth/2-200,650,400,200);
	
	public static Sound menuMusic = new Sound("Menumusic.wav");
	
	public Menu(int wWidth, int wHeight) {
		this.w = wWidth;
		this.h = wHeight;
		menuMusic.play();
		
	}
	
	public void update(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		
		if(!Game.isBlack) g.setColor(Color.DARK_GRAY);g.fillRect(0, 0, w, h);
		
		Font fnt0 = new Font("arial", Font.BOLD, 50);
		g.setFont(fnt0);
		g.setColor(Color.white);
		g.drawString("Lothader", w/2 - 125, 200);
		
		Font fnt1 = new Font("arial", Font.BOLD,120);
		g.setFont(fnt1);
		g.drawString("Start", playButton.x+60, playButton.y+125);
		g.drawString("Mode", modeButton.x+50, modeButton.y+125);
		g2d.draw(modeButton);
		g2d.draw(playButton);
	}
	
	
	
}
