package com.s3dEngine.main;

import java.util.ArrayList;

public class Weapon{
	
	public int[][] map;
	public int mapWidth, mapHeight, width, height;
	public ArrayList<Texture> textures;
	
	private int bullet = 5;
	private Sound gunSound;

	public Weapon(int[][] m, int mapW, int mapH,  ArrayList<Texture> tex, int w, int h) {
		map = m;
		mapWidth = mapW;
		mapHeight = mapH;
		textures = tex;
		width = w;
		height = h;
		gunSound = new Sound("Gunshot.wav");

	}
	
	public int[] update(Camera camera, int[] pixels) {
		
		//nur wenn Space gedrckt wurde
		if(InputHandler.shoot && bullet>=1) {
			bullet--;
			InputHandler.shoot = false;
			//Position des Spielers:
			int mapX = (int) camera.xPos;
			int mapY = (int) camera.yPos;
			
			//Sichtrichtung: - Double, da sonst nicht schrg schieen 
			double rayDirX = camera.xDir;
			double rayDirY = camera.yDir;
			
			//Schnittpunkte mit den Achsen:
			double sideDistX;
			double sideDistY;
			
			//Lnge von einem Kstchen zum nchsten:
		    double deltaDistX = Math.sqrt(1 + (rayDirY*rayDirY) / (rayDirX*rayDirX));
		    double deltaDistY = Math.sqrt(1 + (rayDirX*rayDirX) / (rayDirY*rayDirY));
		    
		    //Distanz zur Wand (nicht schrg)
		    double perpWallDist;
			
		    /*
		     * Beschreibt die Richtung des Rays - In Positive X oder negative Richtung
		     *  x pos = 1, x neg = -1
		     */
		    int stepX, stepY;
		    
		    //Wand Treffer:
		    boolean hit = false;
		    
		    //Derzeitige Achse: 0 = x-Achse
		    int side=0;
		    
		    //Anhand des Rays stepX herausfinden:
		    if(rayDirX < 0) {
		    	
		    	stepX = -1;
		    	
		    	/*SideDist ausrechnen:
		    	 * (camera.xPos - mapX) = Dezimalzahl der x-Koordinate = Startpunkt des Rays
		    	 */
		    	sideDistX = (camera.xPos - mapX) * deltaDistX;
		    }
		    else {
		    	
		    	stepX = 1;
		    	sideDistX = (mapX + 1.0 - camera.xPos) * deltaDistX;
		    }
		    
		    //Selbe in y-Richtung:
		    if (rayDirY < 0)
		    {
		    	stepY = -1;
		        sideDistY = (camera.yPos - mapY) * deltaDistY;
		    }
		    else
		    {
		    	stepY = 1;
		        sideDistY = (mapY + 1.0 - camera.yPos) * deltaDistY;
		    }

		    //Nach einem Treffer an der Wand suchen:
		    while(!hit) {
		    	
		    	if(sideDistX < sideDistY) {
		    		
		    		sideDistX += deltaDistX;
		    		//Der Ray verndert seine X und Y Koordinate
		    		mapX += stepX;
		    		side = 0; //da x-Achse
		    	}
		    	else {
		    		
		    		sideDistY += deltaDistY;
		    		mapY += stepY;
		    		side = 1;	//da y-Achse
		    	}
		    	//Abgleichen mit map Array
		    	if(map[mapX][mapY] > 0) {
		    		hit = true;	//Treffer 
		    		System.out.println("Treffer bei " + map[mapX][mapY]);
		    		if(Game.map[mapX][mapY] > 1 && Game.map[mapX][mapY] < 5) {
		    			Game.map[mapX][mapY] = 0;	//(int)(Math.random()*4)+1
		    		}if(Game.map[mapX][mapY] == 5) {
		    			Game.state = Game.STATE.FINISHED;
		    		}
		    	}
		    	
		    }
		    
			gunSound.play();
		}
		return pixels;	
	}
}

