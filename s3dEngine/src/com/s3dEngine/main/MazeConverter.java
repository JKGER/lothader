package com.s3dEngine.main;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/*
 * Dieser Code wurde auch eigens erstellt und deshalb nirgendswo abgeschaut.
 */

public class MazeConverter {
	
	private int imgHeight,imgWidth;
	private int[][] mazemap;
	
	private URL location;
	private ClassLoader loader = ClassLoader.getSystemClassLoader();

	public MazeConverter(int mazeHeight, int mazeWidth,int[][]maze){
		imgHeight = mazeHeight;
		imgWidth = mazeWidth;
		mazemap = maze;
		
		convert();
	}
	
	private void convert() {
		
		try {
			BufferedImage buffImg = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);
			
			for(int x=0;x<imgWidth;x++) {
				for(int y=0;y<imgHeight;y++) {
					buffImg.setRGB(x, y, Color.BLACK.getRGB());
					if(mazemap[y][x]==0) {
						buffImg.setRGB(x, y, Color.white.getRGB());
					}
					if(mazemap[y][x]==5) {
						buffImg.setRGB(x, y, Color.blue.getRGB());
					}
				}
			}

			
			//Speichern
		//	File file = new File("mapoutput/mazemap.png");
		//	ImageIO.write(buffImg,"png",file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
